# Aviator template

![](themes/aviator/screenshot.png)

[Fork](https://gitlab.com/lukasticky/aviator-template/-/forks/new) this repository to get started. Information about the Aviator theme can be found [here](https://gitlab.com/lukasticky/aviator).

Use this command to update the theme to the latest version:

```
git submodule update --recursive --remote
```
